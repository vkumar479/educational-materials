"""
dp library

The dp library is a simple numpy based library with the purpose of teaching about
computational graphs, automatic differentiation, backpropagation, simple neural network and optimizer.
The library allows to build simple computional grahps up to more complex neural networks all based on 
a set of implementet basic matrix operations. It also includes different initializer to set up
the networks and different optimizer to train them. 




MIT licence 

Copyright 2019 Christian Herta

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the “Software”), to deal in the 
Software without restriction, including without limitation the rights to use, copy, 
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies 
or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

 

import numpy as np

import uuid
import numbers
import operator
import copy 

from types import SimpleNamespace
from collections import namedtuple


# if there are multiple path to a node
# we must add the grad values 
def combine_dicts(a, b, op=operator.add):
    x = (list(a.items()) + list(b.items()) +
        [(k, op(a[k], b[k])) for k in set(b) & set(a)])
    return {x[i][0]: x[i][1] for i in range(0, len(x))}

class Node(object):
    """
    The Node class represents a single neuron, smallest unit in a computional 
    graph. For the purpose of teaching this libary only works with 2D arrays, matrices
    as neurons.
    
    Each node comprises a name, a value and a gradient function, which is key for 
    the ability of automatic differentiation of this class.
    
    The Node class also contains a set of basic operations for node objects. Each operation leads to a new node with a certain value and gradiant 
    function depending on the nodes given and the type of operation.
    
    The Node class alone can be used to teach and learn about computational graphs and backpropagation.
    
    """
    
    NodeStore = dict()

    def _set_variables(self, name, value):
        self.value = value
        self.shape = value.shape
        self.dtype = value.dtype
        self.name = name
        self.uuid = uuid.uuid4()
        if name:
            self.grad = lambda g: {name: g}
            self._register()
        else:
            self.grad = lambda g: {}

    def _test_shape(self):
        # only 2D-Arrays are supported at moment
        assert len(self.shape) == 2


    def __init__(self, value, name=None):
        
        """
        Node object initializer. It will initialize a node object by setting value , name and 
        gradient function and also adjusts the shape to a 2D array if necessary.
        """
        
        if isinstance(value, numbers.Number):
            value = np.array([[value]])
        assert isinstance(value, np.ndarray)
        if len(value.shape) == 1:
            value = value.reshape(-1, 1)

        self._set_variables(name, value)
        self._test_shape()

    def _register(self):
        Node.NodeStore[self.name] = self

    @staticmethod
    def get_param():
        param = dict()
        for n in Node.NodeStore:
            param[n] = Node.NodeStore[n].value
        return param

    @staticmethod
    def set_param(param):
        for n in Node.NodeStore:
            Node.NodeStore[n].value = param[n]
        
    def _broadcast_g_helper(o, o_):# broadcasting make things slightly more complicated
        if o_.shape[0] > o.shape[0]:
            o_ = o_.sum(axis=0).reshape(1,-1)
        if o_.shape[1] > o.shape[1]:
            o_ = o_.sum(axis=1).reshape(-1,1)
        return o_    
                  
    def _set_grad(self_, g_total_self, other, g_total_other):
        g_total_self = Node._broadcast_g_helper(self_, g_total_self)
        x = self_.grad(g_total_self)
        g_total_other = Node._broadcast_g_helper(other, g_total_other)
        x = combine_dicts(x, other.grad(g_total_other))
        return x
                
    def __add__(self, other):
        if isinstance(other, numbers.Number):
            other = Node(np.array([[other]]))
        ret = Node(self.value + other.value)
        def grad(g):
            g_total_self = g
            g_total_other = g
            x = Node._set_grad(self, g_total_self, other, g_total_other)
            return x
        ret.grad = grad
        return ret
    
    def __radd__(self, other):
        return Node(other) + self
    
    def __sub__(self, other):
        if isinstance(other, numbers.Number):
            other = Node(other)
        ret = self + (other * -1.)   
        return ret
    
    def __rsub__(self, other):
        if isinstance(other, numbers.Number):
            other = Node(other) 
            return other - self
        raise NotImplementedError()
        
    def __mul__(self, other):
        if isinstance(other, numbers.Number) or isinstance(other, np.ndarray):
            other = Node(other)           
        ret = Node(self.value * other.value) 
        def grad(g):
            g_total_self = g * other.value
            g_total_other = g * self.value
            x = Node._set_grad(self, g_total_self, other, g_total_other)
            return x
        ret.grad = grad
        return ret      
    
    def __rmul__(self, other):
        if isinstance(other, numbers.Number):
            return Node(other) * self
        raise NotImplementedError()

    def concatenate(self, other, axis=0):
        assert axis in (0,1) # TODO
        ret = Node(np.concatenate((self.value, other.value), axis=axis))
        def grad(g):
            if axis == 0: 
                g_total_self = g[:self.shape[0]] 
                g_total_other = g[self.shape[0]:]
            elif axis == 1:
                g_total_self = g[:, :self.shape[1]] 
                g_total_other = g[:, self.shape[1]:]
            x = Node._set_grad(self, g_total_self, other, g_total_other)
            return x
        ret.grad = grad
        return ret
    
    # slicing
    def __getitem__(self, val):
         raise NotImplementedError()

        
    def __truediv__(self, other):
        if isinstance(other, numbers.Number):
            other = Node(np.array([[other]]))
        ret = Node(self.value / other.value)

        def grad(g):
            g_total_self = g / other.value
            g_total_other = -1. * self.value * g / (other.value**2)
            x = Node._set_grad(self, g_total_self, other, g_total_other)
            return x
        ret.grad = grad
        return ret
    
    def __rtruediv__(self, other):
        if isinstance(other, numbers.Number):
            other = Node(other)
            return other/self
        raise NotImplementedError()
        
    def __neg__(self):
        return self * -1.
    
    def dot(self, other):
        ret = Node(np.dot(self.value, other.value))    
        def grad(g):
            g_total_self = np.dot(g, other.value.T)
            g_total_other = np.dot(self.value.T, g)
            x = Node._set_grad(self, g_total_self, other, g_total_other)
            return x
        ret.grad = grad
        return ret
    
    def transpose(self):
        ret = Node(self.value.T)
        def grad(g):
            x = self.grad(g.T)
            return x
        ret.grad = grad
        return ret    

    T = transpose

    def exp(self):
        ret = Node(np.exp(self.value))
        def grad(g):
            assert self.shape == g.shape
            x = self.grad(np.exp(self.value) * g)
            return x
        ret.grad = grad
        return ret    
    
    def log(self):
        ret = Node(np.log(self.value))
        def grad(g):
            assert self.shape == g.shape
            x = self.grad(1./self.value * g)
            return x
        ret.grad = grad
        return ret     
       
    def square(self):
        ret = Node(np.square(self.value))
        def grad(g):
            assert self.shape == g.shape
            x = self.grad(2 * self.value * g)
            return x
        ret.grad = grad
        return ret       
         
    def sqrt(self):
        ret = Node(np.sqrt(self.value))
        def grad(g):
            assert self.shape == g.shape
            x = self.grad(0.5 * (1/np.sqrt(self.value)) * g)
            return x
        ret.grad = grad
        return ret     
    
    def tanh(self):
        ret = Node(np.tanh(self.value))
        def grad(g):
            assert self.shape == g.shape
            x = self.grad((1 - np.square(np.tanh(self.value))) * g)
            return x
        ret.grad = grad
        return ret
    
    def sum(self, axis=None):
        if axis is None:
            return self._sum_all()
        assert axis in (0,1)
        return self._sum(axis)
        
    def _sum_all(self):
        ret = Node(np.sum(self.value).reshape(1,1))
        def grad(g):
            x = self.grad(np.ones_like(self.value) * g)
            return x
        ret.grad = grad
        return ret
    
    def _sum(self, axis):
        ret = self.value.sum(axis=axis)
        if axis==0: 
            ret = ret.reshape(1, -1)
        else:
            ret = ret.reshape(-1, 1)
        ret = Node(ret)
        def grad(g):
            x = self.grad(np.ones_like(self.value) * g)
            return x
        ret.grad = grad
        return ret  
    
    def relu(self):
        self.mask = self.value > 0.
        ret = Node(self.mask * self.value)
        def grad(g):
            assert self.shape == g.shape
            x = self.grad(self.mask * g)
            return x
        ret.grad = grad
        return ret    
    
    def leaky_relu(self, slope = 0.01):
        self.mask = self.value>0.
        ret = Node(self.mask*self.value + slope*(self.value - self.value*self.mask))
        def grad(g):
            assert self.shape == g.shape
            x = self.grad(self.mask * g + (slope*(g - self.mask*g )))
            return x
        ret.grad = grad
        return ret
    
    def softmax(self):
        ret = self.exp() / self.exp().sum(axis=1)
        np.testing.assert_almost_equal(ret.value.sum(axis=1), 1.)
        return ret
    
    def sigmoid(self):# bug
        return 1./(1. + self.__neg__().exp())

    def reshape(self, shape):
        ret = Node(self.value.reshape(shape))
        def grad(g):
            x = self.grad(g.reshape(self.shape))
            return x
        ret.grad = grad
        return ret

class NeuralNode(object):
    """
    The NeuralNode class initializes a set of parameters (Node objects) which represent  
    a layer in the neural network. Basically the class initializes weights W and biases b
    which are the building blocks for a linear transformation via Wx+b in the provided linear layer. 
    For the initialisation the class provides a ....  Xavier and pure uniform initialisation.
    
    """
     
    _sep = "_"
    #param = dict()
    #def register():
    #    NeuralNode.param = Node.get_param()

    @staticmethod
    def _get_fullname(name, suffix):
        return NeuralNode._sep.join([name, suffix])

    @staticmethod
    def _initialize_W(fan_in, fan_out):
        """
        This function performs a default initialisation for the weights.
        
        Args: 
            fan_in:     number of layer inputs
            fan_out:    number of layer outputs
            
        Returns:
            initial_W:  initial weights         
        """
        
        gain = np.sqrt(2)
        std = gain / np.sqrt(fan_in)
        bound = np.sqrt(3) * std 
        initial_W = np.random.uniform(-bound, bound, size=(fan_in, fan_out))
        return initial_W

    @staticmethod
    def _initialize_b(fan_in, fan_out):
        """
        This function performs a default initialisation for the diases.
        
        Args: 
            fan_in:     number of layer inputs
            fan_out:    number of layer outputs
            
        Returns:
            initial_b:  initial weights         
        """
        bound = 1 / np.sqrt(fan_in)
        initial_b = np.random.uniform(-bound, bound, size=(1, fan_out))  
        return initial_b
    
    # --------------------------- Other initialization methods -------------------
    @staticmethod
    def Xavier(fan_in, fan_out):
        """
        This function performs a Xavier initialisation for the weights and biases.
        
        Args: 
            fan_in:     number of layer inputs
            fan_out:    number of layer outputs
            
        Returns:
            initial_W:  initial weights 
            initial_b:  initial biases 
        """
        
        gain=np.sqrt(2)
        bound = gain * np.sqrt(6/(fan_in+fan_out))
        inital_W = np.random.uniform(-bound, bound, size=(fan_in, fan_out))     
        bound = 1 / np.sqrt(fan_in)
        inital_b = np.random.uniform(-bound, bound, size=(1, fan_out))
         
        return inital_W, inital_b
    
    
    @staticmethod
    def NormalInitializer(fan_in, fan_out):
        """
        This function performs a normal value initialisation for the weights and biases.
        Drawn from a uniform distribution. 
        
        Args: 
            fan_in:     number of layer inputs
            fan_out:    number of layer outputs
            
        Returns:
            initial_W:  initial weights 
            initial_b:  initial biases 
        """
        
        bound = 1/np.sqrt(fan_in)
        inital_W = np.random.uniform(-bound, bound, size=(fan_in, fan_out))
        inital_b = np.random.uniform(-bound, bound, size=(1, fan_out))
         
        return inital_W, inital_b
    

    
    # ------------------------------------------------------------------------

    @staticmethod
    def get_name_and_set_param(param, layer_name, ext_name):
        node_name = NeuralNode._get_fullname(layer_name, ext_name)
        node_value = param.get(node_name)
        return node_name, node_value

    
    def _Linear_Layer(fan_in, fan_out, name=None, param=dict(), initializer=None):
        """
        This functions initialises weights and biases and performs an affine transformation 
        on the input. Represents a pure linear layer in a neural network.
        
        Args:
            fan_in:         number of layer inputs
            fan_out:        number of layer outputs
            initializer:    initializer used for the initialization of weights and biases
        
        Kwargs:
            name:           name of the layer, used for the parameter dictionary
            param:          parameter dictionary, includes all weights and biases 
        
        Returns:
            lambda X:       function representing the affine transformation
            param:          dictionary consisting of all parameters in the neural net
            nodes:          dictionary consisting of all weight and biases for this specific linear layer
        
        """
        
        assert isinstance(name, str) 
        weight_name, W_value = NeuralNode.get_name_and_set_param(param, name, "weight")
        bias_name, b_value = NeuralNode.get_name_and_set_param(param, name, "bias")
        
        assert (W_value is None and b_value is None) or (W_value is not None and b_value is not None)
        if W_value is None:
           
            if initializer is None:
                W_value = NeuralNode._initialize_W(fan_in, fan_out)
                b_value = NeuralNode._initialize_b(fan_in, fan_out)
            
            if initializer == "Xavier":
                W_value, b_value = NeuralNode.Xavier(fan_in, fan_out)
                
            if initializer == "NormalInitializer":
                W_value, b_value = NeuralNode.NormalInitializer(fan_in,fan_out)
                
            # fall-through
            if W_value is None and b_value is None:
                print("WARNING: Unrecognized initializer '{}'. Using 'Xavier' instead.".format(initializer))
                W_value, b_value = NeuralNode.Xavier(fan_in, fan_out)

            param[weight_name] = W_value
            param[bias_name] = b_value
            
        W = Node(W_value, weight_name)
        b = Node(b_value, bias_name)
        nodes = dict()  # type: Dict[str, Node]
        nodes[weight_name] = W
        nodes[bias_name] = b
        return lambda X: (X.dot(W) + b), param, nodes
    
    def _ReLu_Layer(fan_in, fan_out, name=None, param=dict(), initializer=None):
        """
        This functions builds a Relu layer for a neural network by setting up an linar layer and
        connecting it with a relu function.
        
        Args:
            fan_in:         number of layer inputs
            fan_out:        number of layer outputs
            initializer:    initializer used for the initialization of weights and biases
        
        Kwargs:
            name:           name of the layer, used for the parameter dictionary
            param:          parameter dictionary, includes all weights and biases 
        
        Returns:
            f:              function representing the affine transformation connected with a relu function
            param:          dictionary consisting of all parameters in the neural net
            nodes:          dictionary consisting of all weight and biases for this specific linear layer
        
        """
        
        ll, param, nodes = NeuralNode._Linear_Layer(fan_in, fan_out, name, param)
        f = lambda X: ll(X).relu()
        return f, param, nodes

# short namespace name        
nn = NeuralNode 


class Model(object):  
        """
        Model is the parent class for designing our neural net.
        
        """
        # nodes are neural nodes
        _NNode = namedtuple("NNode", ['layer_type', 'param', 'nodes'])  # type: Type[NNode]
        
        def __init__(self):
            self.neural_nodes = dict()
            
        # params are not structured in nodes
        def get_param(self):
            """
            Retruns a dictionary of all parameters (weights and bias) used in the network.
            
            Returns:
                A dictionary
            """
            param = dict()
            for node_name, node_value in self.neural_nodes.items():
                param = {**param, **node_value.param} # merge dicts
            return param
        
       
        def set_param(self, param):
            """
            Sets the values of parameters used in the neural network. New values are given by 
            the param dictinary.
            
            Args:
                param:    dictionary, contains new parameter values
            
        
            """
            for node_name, node_value in self.neural_nodes.items():
                for param_name in node_value.param:
                    node_value.param[param_name] = param[param_name]
            
        def get_grad(self, x, y):
            """
            Calculates and returns the current gradient and value of the loss function .
            
            Args:
                x:    x training data (features)
                y:    y data (prediction) 
                    
            Returns:
                loss_grad(g):   current model gradient
                loss_:          current loss value
            """
            loss_ = self.loss(x, y)
            g = np.ones_like(loss_.value)
            return loss_.grad(g), loss_
        
        
        def _set_layer(self, fan_in, fan_out, name, layer_type, initializer=None):
            """
            Adds a layer to the neural network.
            
            Args:
                fan_in:         number of layer inputs
                fan_out:        number of layer outputs
                name:           String, name of the layer
                layer_type:     layer type defined in the NeuralNode class
                 
            Kwargs:
                initializer:    initializer used for the initialization of weights and biases
            
            Returns:
                lambda x: f(x)         function represented by the layer
            """
            assert isinstance(name, str)
            assert name not in self.neural_nodes.keys()
            f, param, nodes = layer_type(fan_in, fan_out, name=name, initializer=initializer, param=dict())
            self.neural_nodes[name] = Model._NNode(layer_type=layer_type, param=param, nodes=nodes)
            return lambda x: f(x)

        def ReLu_Layer(self, fan_in, fan_out, name=None, initializer=None):
            """
            Adds a Relu layer to the neural network.
            """
            return self._set_layer(fan_in, fan_out, name, NeuralNode._ReLu_Layer, initializer)
        
        def Linear_Layer(self, fan_in, fan_out, name=None, initializer=None ):
            """
            Adds a Linear layer to the neural network.
            """
            return self._set_layer(fan_in, fan_out, name, NeuralNode._Linear_Layer, initializer)
    
        
        def _add_to_params(self, p):
            assert p.keys().isdisjoint(self.params.keys())
            self.param = {**self.param, **p} # merge dicts
 
        # following methods     
        # must be implemented by subclasses
        
        def forward(self, x):
            """
            Has to be implemented when setting up the neural network. Calculates the forward pass
            defined by the neural network.
            """
            raise NotImplementedError()
    
        def loss(self, x, y):
            """
            Loss function which should be optimized by the neural network.
            Has to be implemented when setting up the neural network. 
            """
            raise NotImplementedError()
            
        # example: cross entropy 
        # y should be one hot encoded
        #def loss(self, x, y=None):
        #    n = x.shape[0]
        #    logits = self.logits(x) # has be be defined
        #    # numeric stable max sum exponent   
        #    max_logit = Node(np.max(logits.value, axis=1))
        #    log_softmax = logits - max_logit - (logits - max_logit).exp().sum(axis=1).log()
        #    loss = - y * log_softmax
        #    return loss.sum()/n

##################################################################################################


class Optimizer(object):
    """
    The class Optimizer works as a parent class for all specialized types of Optimizers 
    and provides the basic methods for training the network using an optimizer.
    """
    
    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):
        """
        Initializes an optimizer.
        
        Args:
            model:          neural network which should be optimized
           
        Kwargs:
            x_train:        training data, features
            y_train:        training data
            hyperparam:     dictionary of hyperparameters used be the optimizer
            batch_size:     batch size for neural network training
            
        """
        self.model = model
        self.x_train = x_train
        self.y_train = y_train
        self.batch_size=batch_size
        self.hyperparam = hyperparam
        self._set_param()
        self.grad_stores = []  # list of dicts for momentum, etc. 
        self.loss_hist = []    # list for the documentation of loss history
        self.para_hist = []    # list for the documentation of parameter/network development
        
    def random_batch(self):
        """
        Samples a random batch from the given training data.
        
        Returns:
            Random training batch
            
        """
        n = self.x_train.shape[0]
        indices = np.random.randint(0, n, size=self.batch_size)
        return Node(self.x_train[indices]), Node(self.y_train[indices])

    def train(self, steps=1000, print_each=100):
        raise NotImplementedError()

    def _train(self, steps=1000, num_grad_stores=0, print_each=100,hist=False):
        """
        This function performs the training loop, which optimizes the paramters of the 
        model.
        
        Kwargs:
            steps:              number of epochs
            num_grad_stores:    number of gradients stored for momentum approaches
            print_each:         number of epochs after which the lost is printed
            hist:               bool, for TRUE parameter and loss history are returned
            
        Returns:
            if hist=True:
                loss.value:     loss function value from last step
                loss_hist:      loss function value history from training
                para_hist:      history of model parameter development

        """
        assert num_grad_stores in (0,1,2)
        model = self.model
        if num_grad_stores>0:
            x, y = self.random_batch()
            grad, loss = model.get_grad(x, y)
            self.grad_stores = [dict() for _ in range(num_grad_stores)]
        for grad_store in self.grad_stores:
            for g in grad:
                grad_store[g] = np.zeros_like(grad[g])
                
        param = model.get_param()
        
        print("iteration\tloss")    
        for i in range(1, steps+1):
            x, y = self.random_batch()
            grad, loss = model.get_grad(x, y)
  
            self.loss_hist.append(loss.value[0,0])
            
            para_item = model.get_param()
            self.para_hist.append(copy.deepcopy(para_item))
            
            if i%print_each==0 or i==1:
                print(i, "\t",loss.value[0,0])   
                #print(para_item)
                
            for g in grad:
                #assert param[g].shape == self.grad_stores[0][g].shape
                #param[g] =
                self._update(param, grad, g, i)
            
            model.set_param(param)
        
        if hist==True:
            print('wiht hist')
            return loss.value, self.loss_hist, self.para_hist
        
        if hist==False:
            return loss.value




class SGD(Optimizer):
    """
    This class sets up a simple Stochastic Gradient Descent optimizer.
    """
    
    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):
        super(SGD, self).__init__(model, x_train, y_train, hyperparam, batch_size)
        
    def _set_param(self):
        """
        Sets the hyperparameters used by this optimizer.
        """
        ## Learning rate 
        self.alpha = self.hyperparam.get("alpha", 0.001)
        

    def _update(self, param, grad, g, i):
        """
        Update rule for a simple Stochastic Gradient Descent (SGD) training.
        
        Args:
            param:       dictionary containing the model parameters
            grad:        dictionary containing the gradients of model parameters
            g:           name of actual parameter to optimize
            i:           actual traning epoch
        """
        param[g] -= self.alpha * grad[g]    
             
    def train(self, steps=1000, print_each=100,err_hist=False):
        """
        Starts the training using the method Optimizer._train and the update rule defined 
        in SGD._update.
        """
        return self._train(steps, num_grad_stores=1, print_each=print_each,hist=err_hist)

optimizer = SimpleNamespace()
optimizer.SGD = SGD
     
class SGD_Momentum(Optimizer):
    """
    This class sets up a Stochastic Gradient Descent optimizer with momentum.
    """
    
    
    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):
        super(SGD_Momentum, self).__init__(model, x_train, y_train, hyperparam, batch_size)
        
    def _set_param(self):
        """
        Sets the hyperparameters used by this optimizer.
        """
        ## Learning rate
        self.alpha = self.hyperparam.get("alpha", 0.001)
        ## beta for momentum term 
        self.beta = self.hyperparam.get("beta", 0.9) 
    
    def _update(self, param, grad, g, i):
        """
        Update rule for a Stochastic Gradient Descent with momentum training.
        
        Args:
            param:       dictionary containing the model parameters
            grad:        dictionary containing the gradients of model parameters
            g:           name of actual parameter to optimize
            i:           actual traning epoch
            
        """
        
        gradients = self.grad_stores[0]
        gradients[g] = self.beta * gradients[g] + (1-self.beta) * grad[g]
        #gradients[g] /= (1. - self.beta**i) # bias correction
        param[g] -= self.alpha * gradients[g]    
        #return param[g]
    
    def train(self, steps=1000, print_each=100,err_hist=False):
        """
        Starts the training using the method Optimizer._train and the update rule defined 
        in SGD_Momentum._update.
        """
        return self._train(steps, num_grad_stores=1, print_each=print_each,hist=err_hist)
    

optimizer.SGD_Momentum = SGD_Momentum

class RMS_Prop(Optimizer):
    """
    This class sets up a RMSProp (Root Mean Square Propagation) optimizer .
    """
    
    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):
        super(RMS_Prop, self).__init__(model, x_train, y_train, hyperparam, batch_size)
    
    def _set_param(self):
        """
        Sets the hyperparameters used by this optimizer.
        """
        self.alpha = self.hyperparam.get("alpha", 0.001)
        self.beta2 = self.hyperparam.get("beta2", 0.99) 
        self.epsilon = self.hyperparam.get("epsilon", 10e-8) 
    
    def _update(self, param, grad, g, i):
        """
        Update rule for a RMSProp training.
        
        Args:
            param:       dictionary containing the model parameters
            grad:        dictionary containing the gradients of model parameters
            g:           name of actual parameter to optimize
            i:           actual traning epoch
            
        """
        squared_gradients = self.grad_stores[0]
        squared_gradients[g] = self.beta2 * squared_gradients[g] + (1-self.beta2) * (grad[g])**2
        squared_gradient_corrected = squared_gradients[g]/(1. - self.beta2**i) # bias correction
        param[g] -= self.alpha * grad[g]/np.sqrt(squared_gradient_corrected+self.epsilon)   
        #return param[g]
    
    def train(self, steps=1000, print_each=100,err_hist=False):
        """
        Starts the training using the method Optimizer._train and the update rule defined 
        in RMS_Prop._update.
        """
        return self._train(steps, num_grad_stores=1, print_each=print_each,hist=err_hist)

optimizer.RMS_Prop = RMS_Prop

class Adam(Optimizer):
    
    def __init__(self, model, x_train=None, y_train=None, hyperparam=dict(), batch_size=128):
        super(Adam, self).__init__(model, x_train, y_train, hyperparam, batch_size)
    
    def _set_param(self):
        """
        Sets the hyperparameters used by this optimizer.
        """
        self.alpha = self.hyperparam.get("alpha", 0.001)
        self.beta = self.hyperparam.get("beta", 0.9) 
        self.beta2 = self.hyperparam.get("beta2", 0.99)
        self.epsilon = self.hyperparam.get("epsilon", 10e-8)
    
    def _update(self, param, grad, g, i):
        """
        Update rule for an Adam training.
        
        Args:
            param:       dictionary containing the model parameters
            grad:        dictionary containing the gradients of model parameters
            g:           name of actual parameter to optimize
            i:           actual traning epoch
            
        """
        velocity = self.grad_stores[0]
        squared_grads = self.grad_stores[1]
        
        # update velocity
        v = self.beta * velocity[g] + (1 - self.beta) * grad[g]
        np.copyto(velocity[g], v)
        
        # update squared grads
        sq = self.beta2 * squared_grads[g] + (1 - self.beta2) * np.square(grad[g])
        np.copyto(squared_grads[g], sq)
        
        # bias correction
        v_corrected = v/(1 - self.beta**i)
        sq_corrected = sq/(1 - self.beta2**i)
        
        # update network
        update = param[g] - self.alpha * (v_corrected/np.sqrt(sq_corrected + self.epsilon))
        np.copyto(param[g], update)
    
    def train(self, steps=1000, print_each=100,err_hist=False):
        """
        Starts the training using the method Optimizer._train and the update rule defined 
        in Adam._update.
        """
        return self._train(steps, num_grad_stores=2, print_each=print_each,hist=err_hist)

# not tested right now
optimizer.Adam = Adam

#####################################################################################################

class Helper():

    @staticmethod
    def one_hot_encoding(y, nb_classes):
        m = y.shape[0]
        y_ = np.zeros((m, nb_classes), dtype=int)
        y_[np.arange(m), y.astype(int)] = 1
        return y_
